# -*- coding: utf-8 -*-


class Reloj:

    def __init__(self, hour, mins):
        self.hour = hour % 24
        self.min = mins % 60
        if self.hour < 0:
            self.hour += 24
        if self.min < 0:
            self.min += 60

    def suma(self, num):
        self.hour += num
        self.hour %= 24
        if self.hour < 0:
            self.hour += 24

    def __str__(self):
        return "%02d:%02d" % (self.hour, self.min)


assert str(Reloj(8, 0)) == '08:00'
assert str(Reloj(72, 8640)) == '00:00'
assert str(Reloj(-1, 15)) == '23:15'
assert str(Reloj(-25, -160)) == '23:20'
r = Reloj(10, 3)
r.suma(-70)
assert str(r) == '12:03'
