
1) Para cada una de las llamadas:
- x(''): Retorna 1
- x('1 2 3 4'): Retorna 4
- x('1 2 3 4 A'): Retorna 4
- x('hola?'): Retorna 3
- x('HOLA'): Retorna 4

La función X elimina los espacios de un string, y luego le aplica Y en caso de ser no vacio
La función Y verifica que todos los caracteres alfabéticos sean mayúsculas, con excepción de la última letra. En caso contrario le aplica Z.
La función Z verifica que la última letra sea '?'.

2) Puede producir un race-condition sobre tratar de aplicar educe sobre un arreglo mal definido. Se debe asegurar el correcto manejo de errores con escepciones.

3) Comentar las funciones, la implementación de Z es excesiva. Se puede implementar su lógica dentro de Y.
   Los retornos no aportan información al uso de las funciones. Cada una de las funcionalidades atómicas se pueden separar en funciones más pequñas, teniendo a un método que las llame.